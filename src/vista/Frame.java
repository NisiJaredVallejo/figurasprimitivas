package vista;

import controlador.Controlador;

public class Frame extends javax.swing.JFrame {

    public Frame() {
        initComponents();
    }

    public void addEventos() {
        Controlador controlador = new Controlador(this);
        miAbrir.addActionListener(controlador);
        miNuevo.addActionListener(controlador);
        miGuardar.addActionListener(controlador);
        bgFiguras.add(rbLinea);
        bgFiguras.add(rbTriangulo);
        bgFiguras.add(rbRectangulo);
        bgFiguras.add(rbPentagono);
        panelPrincipal.addMouseListener(controlador);
        spG.addChangeListener(controlador);
        spB.addChangeListener(controlador);
        btMasRotar.addActionListener(controlador);
        btMenosRotar.addActionListener(controlador);
        btMasTamanio.addActionListener(controlador);
        btMenosTamanio.addActionListener(controlador);
        btMovimientoArriba.addActionListener(controlador);
        btMovimientoAbajo.addActionListener(controlador);
        btMovimientoDerecha.addActionListener(controlador);
        btMovimientoIzquierda.addActionListener(controlador);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgFiguras = new javax.swing.ButtonGroup();
        rbLinea = new javax.swing.JRadioButton();
        rbTriangulo = new javax.swing.JRadioButton();
        rbRectangulo = new javax.swing.JRadioButton();
        rbPentagono = new javax.swing.JRadioButton();
        lbContador = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        pnColor = new javax.swing.JPanel();
        spB = new javax.swing.JSpinner();
        spG = new javax.swing.JSpinner();
        spR = new javax.swing.JSpinner();
        btMovimientoAbajo = new javax.swing.JButton();
        btMovimientoArriba = new javax.swing.JButton();
        btMovimientoDerecha = new javax.swing.JButton();
        btMovimientoIzquierda = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cbFiguras = new javax.swing.JComboBox<>();
        btMasTamanio = new javax.swing.JButton();
        btMasRotar = new javax.swing.JButton();
        btMenosTamanio = new javax.swing.JButton();
        btMenosRotar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panelPrincipal = new vista.Panel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miNuevo = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miAbrir = new javax.swing.JMenuItem();
        miGuardar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 153, 255));
        setResizable(false);
        getContentPane().setLayout(null);

        rbLinea.setSelected(true);
        getContentPane().add(rbLinea);
        rbLinea.setBounds(10, 30, 20, 30);
        getContentPane().add(rbTriangulo);
        rbTriangulo.setBounds(10, 60, 20, 30);
        getContentPane().add(rbRectangulo);
        rbRectangulo.setBounds(10, 90, 20, 30);
        getContentPane().add(rbPentagono);
        rbPentagono.setBounds(10, 120, 21, 30);

        lbContador.setText("Puntos: 0");
        getContentPane().add(lbContador);
        lbContador.setBounds(30, 580, 60, 20);
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 480, 0, 0);

        jLabel13.setText("Color:");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(20, 180, 40, 14);

        jLabel14.setText("B:");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(100, 230, 20, 20);

        jLabel15.setText("R:");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(20, 230, 20, 20);

        jLabel16.setText("G:");
        getContentPane().add(jLabel16);
        jLabel16.setBounds(60, 230, 20, 20);

        jLabel17.setText("Figuras:");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(20, 10, 60, 14);

        pnColor.setBackground(new java.awt.Color(204, 153, 255));

        javax.swing.GroupLayout pnColorLayout = new javax.swing.GroupLayout(pnColor);
        pnColor.setLayout(pnColorLayout);
        pnColorLayout.setHorizontalGroup(
            pnColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        pnColorLayout.setVerticalGroup(
            pnColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        getContentPane().add(pnColor);
        pnColor.setBounds(70, 180, 20, 20);

        spB.setModel(new javax.swing.SpinnerNumberModel(106, 0, 255, 1));
        spB.setValue(255);
        getContentPane().add(spB);
        spB.setBounds(80, 260, 40, 20);

        spG.setModel(new javax.swing.SpinnerNumberModel(57, 0, 255, 1));
        spG.setValue(153);
        getContentPane().add(spG);
        spG.setBounds(40, 260, 40, 20);

        spR.setModel(new javax.swing.SpinnerNumberModel(25, 0, 255, 1));
        spR.setValue(204);
        getContentPane().add(spR);
        spR.setBounds(0, 260, 40, 20);

        btMovimientoAbajo.setFont(new java.awt.Font("Monotype Corsiva", 0, 3)); // NOI18N
        btMovimientoAbajo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/flechaAbajo.png"))); // NOI18N
        btMovimientoAbajo.setText("ab");
        btMovimientoAbajo.setBorderPainted(false);
        btMovimientoAbajo.setContentAreaFilled(false);
        getContentPane().add(btMovimientoAbajo);
        btMovimientoAbajo.setBounds(50, 440, 20, 20);

        btMovimientoArriba.setBackground(new java.awt.Color(204, 153, 255));
        btMovimientoArriba.setFont(new java.awt.Font("Monotype Corsiva", 0, 3)); // NOI18N
        btMovimientoArriba.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/flechaArriba.png"))); // NOI18N
        btMovimientoArriba.setText("a");
        btMovimientoArriba.setBorderPainted(false);
        btMovimientoArriba.setContentAreaFilled(false);
        getContentPane().add(btMovimientoArriba);
        btMovimientoArriba.setBounds(50, 400, 20, 20);

        btMovimientoDerecha.setFont(new java.awt.Font("Monotype Corsiva", 0, 3)); // NOI18N
        btMovimientoDerecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/flechaDerecha.png"))); // NOI18N
        btMovimientoDerecha.setText("d");
        btMovimientoDerecha.setBorderPainted(false);
        btMovimientoDerecha.setContentAreaFilled(false);
        getContentPane().add(btMovimientoDerecha);
        btMovimientoDerecha.setBounds(70, 420, 20, 20);

        btMovimientoIzquierda.setFont(new java.awt.Font("Monotype Corsiva", 0, 3)); // NOI18N
        btMovimientoIzquierda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/flechaIzquierda.png"))); // NOI18N
        btMovimientoIzquierda.setText("i");
        btMovimientoIzquierda.setBorderPainted(false);
        btMovimientoIzquierda.setContentAreaFilled(false);
        btMovimientoIzquierda.setName("i"); // NOI18N
        getContentPane().add(btMovimientoIzquierda);
        btMovimientoIzquierda.setBounds(30, 420, 20, 20);

        jLabel3.setText("Transformaciones:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 310, 100, 14);

        cbFiguras.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Figura..." }));
        cbFiguras.setToolTipText("");
        getContentPane().add(cbFiguras);
        cbFiguras.setBounds(10, 340, 90, 20);

        btMasTamanio.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        btMasTamanio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btMasTamanio.setText("+t");
        btMasTamanio.setBorderPainted(false);
        btMasTamanio.setContentAreaFilled(false);
        getContentPane().add(btMasTamanio);
        btMasTamanio.setBounds(20, 540, 30, 20);

        btMasRotar.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        btMasRotar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btMasRotar.setText("+r");
        btMasRotar.setBorderPainted(false);
        btMasRotar.setContentAreaFilled(false);
        getContentPane().add(btMasRotar);
        btMasRotar.setBounds(20, 490, 30, 20);

        btMenosTamanio.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        btMenosTamanio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menos.png"))); // NOI18N
        btMenosTamanio.setText("-t");
        btMenosTamanio.setBorderPainted(false);
        btMenosTamanio.setContentAreaFilled(false);
        getContentPane().add(btMenosTamanio);
        btMenosTamanio.setBounds(60, 540, 30, 20);

        btMenosRotar.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        btMenosRotar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menos.png"))); // NOI18N
        btMenosRotar.setText("-r");
        btMenosRotar.setBorderPainted(false);
        btMenosRotar.setContentAreaFilled(false);
        getContentPane().add(btMenosRotar);
        btMenosRotar.setBounds(60, 490, 30, 20);

        jLabel1.setText("Línea");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 40, 50, 14);

        jLabel2.setText("Triangulo");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(40, 70, 60, 14);

        jLabel5.setText("Rectangulo");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(40, 100, 70, 14);

        jLabel10.setText("Pentagono");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(40, 130, 70, 14);

        jLabel8.setText("Translacion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(30, 380, 80, 14);

        jLabel9.setText("Rotacion");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(40, 470, 60, 14);

        jLabel11.setText("Escalacion");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(40, 520, 70, 14);

        panelPrincipal.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 760, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
        );

        getContentPane().add(panelPrincipal);
        panelPrincipal.setBounds(130, 0, 760, 630);

        jMenuBar1.setBackground(new java.awt.Color(204, 153, 255));

        jMenu1.setText("Archivo");

        miNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        miNuevo.setText("Nuevo");
        jMenu1.add(miNuevo);
        jMenu1.add(jSeparator1);

        miAbrir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        miAbrir.setText("Abrir");
        jMenu1.add(miAbrir);

        miGuardar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        miGuardar.setText("Guardar");
        jMenu1.add(miGuardar);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.ButtonGroup bgFiguras;
    public javax.swing.JButton btMasRotar;
    public javax.swing.JButton btMasTamanio;
    public javax.swing.JButton btMenosRotar;
    public javax.swing.JButton btMenosTamanio;
    private javax.swing.JButton btMovimientoAbajo;
    private javax.swing.JButton btMovimientoArriba;
    private javax.swing.JButton btMovimientoDerecha;
    private javax.swing.JButton btMovimientoIzquierda;
    public javax.swing.JComboBox<String> cbFiguras;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    public javax.swing.JLabel lbContador;
    public javax.swing.JMenuItem miAbrir;
    public javax.swing.JMenuItem miGuardar;
    public javax.swing.JMenuItem miNuevo;
    public vista.Panel panelPrincipal;
    public javax.swing.JPanel pnColor;
    public javax.swing.JRadioButton rbLinea;
    public javax.swing.JRadioButton rbPentagono;
    public javax.swing.JRadioButton rbRectangulo;
    public javax.swing.JRadioButton rbTriangulo;
    public javax.swing.JSpinner spB;
    public javax.swing.JSpinner spG;
    public javax.swing.JSpinner spR;
    // End of variables declaration//GEN-END:variables

}
